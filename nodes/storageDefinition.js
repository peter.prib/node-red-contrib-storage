const logger = new (require("node-red-contrib-logger"))("Storage Definition");
logger.sendInfo("Copyright 2022 Jaroslav Peter Prib");
const noderedBase=require("./noderedBase.js");
const BlockStorage=require("./BlockStorage.js");

module.exports = function (RED) {
	function Node(n) {
	    RED.nodes.createNode(this,n);
		logger.sendInfo({label:"createNode",node:n});
	    const node=Object.assign(this,n);
		node._base=new noderedBase(RED,node);
		try{
			node.status({fill:"yellow",shape:"ring",text:"opening"});
			node._storage=new BlockStorage({fileName:node.fileName})
			.setErrorFunction((ex)=>error(node,ex));
			node.onReady=node._storage.onReady.bind(node._storage);
			node.read=node._storage.read.bind(node._storage);
			node.write=node._storage.write.bind(node._storage);
			node.openFile=node._storage.open.bind(node._storage);
			node.closeFile=node._storage.close.bind(node._storage);
			node.setAvailable=node._storage.setAvailable.bind(node._storage);
			node.setUnavailable=node._storage.setUnavailable.bind(node._storage);
			node._storage.open();
		} catch(ex) {
			node._base.error(ex,"Invalid setup "+ex.message);
			return;
		}
		node.close = function (removed, done) {
			logger.info("closing file");
			node._storage.close(done,(ex)=>{
				error(node,ex,"close "+ex.message);
				done();
			});
		}
	}
	RED.nodes.registerType(logger.label, Node);
};