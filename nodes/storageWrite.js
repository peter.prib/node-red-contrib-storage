const logger = new (require("node-red-contrib-logger"))("Storage Write");
logger.sendInfo("Copyright 2022 Jaroslav Peter Prib");

function error(node,message,shortMessage){
	if(logger.active) logger.send({label:"error",node:node.id,error:error,shortMessage});
	node.error(message);
	node.status({fill:"red",shape:"ring",text:shortMessage});
}

const noderedBase=require("./noderedBase.js");
function debugValue(v){
	return v?(typeof v === 'function'? v.toString() : v):"*** null/undefined ***";
}
module.exports = function (RED) {
	function Node(n) {
	    RED.nodes.createNode(this,n);
	    const node=Object.assign(this,n);
		node._base=new noderedBase(RED,node);
		try{
			node.status({fill:"yellow",shape:"ring",text:"opening"});
			node.storageDefinitionNode=RED.nodes.getNode(node.storageDefinition);
			node.storageDefinitionNode.onReady(node,
				()=>node.status({fill:"green",shape:"ring"}),
				(err)=>node.status({fill:"red",shape:"ring",text:err})
			);
			if(node.storageDefinitionNode==null) throw Error(node.storageDefinition+" storage definition not found")
			this._base.setSource("key").setSource("source")
		} catch(ex) {
			node._base.error(ex,"Invalid setup "+ex.message);
			return;
		}
		node.on("input",function (msg) {
			try{
				const key=node.getKey(msg);
				if(logger.active) logger.send({label:"input",node:node.id,key:key});
				node.storageDefinitionNode.write(key,node.getSource(msg),
					()=>{
						if(logger.active) logger.send({label:"write",node:node.id});
						node.send(msg);
					},
					(ex)=>{
						if(logger.active) logger.send({label:"write",node:node.id,action:node.action,exception:ex.message,stack:ex.stack});
						msg.error=ex.message;
						node.send([null,msg]);
					}
				);
			} catch(ex) {
				msg.error=ex.message;
				node.send([null,msg]);
				if(logger.active) logger.send({label:"write error",node:node.id,exception:ex.message,stack:ex.stack});
			}
		});
	}
	RED.nodes.registerType(logger.label, Node);
};