const logger = new (require("node-red-contrib-logger"))("noderedBase");
logger.sendInfo("Copyright 2022 Jaroslav Peter Prib");

function noderedBase(RED,node){
    this.node=node;
    this.RED=RED;
    this.nodeContext=this.node.context();
    this.flow=this.nodeContext.flow;
    this.global=this.nodeContext.global;

}
noderedBase.prototype.evalFunction=function(id,mapping){
	const node=this.node;
	const nodeContext=this.context;
    const flow=this.flow;
    const global=this.global;
	try{
		return eval(mapping);
	} catch(ex) {
		logger.sendError({label:"evalFunction error",id:id,mapping:mapping,error:ex.message})
		throw Error(id+" "+ex.message);
	}
}
noderedBase.prototype.error=function(ex,shortMessage=ex.message){
	if(logger.active) logger.send({label:"error",node:{id:this.node.id,name:this.node.name},shortMessage:shortMessage,message:ex.message,stack:ex.stack});
	this.node.error(ex.message);
	this.node.status({fill:"red",shape:"ring",text:shortMessage});
}
noderedBase.prototype.evalInFunction=function(propertyName){
	const thisObject=this;
	const node=this.node
	try{
		if(!node.hasOwnProperty(propertyName)) throw Error("no value for "+propertyName);
		const property=this.node[propertyName];
		const propertyType=propertyName+"-type";
		if(!node.hasOwnProperty(propertyType)) throw Error("no value for "+propertyType);
		switch (node[propertyType]){
		case "num":
		case "str":
		case "date":
		case "json":
		case "jsonata":
		case "bin":
		case "bool":
				return this.evalFunction(propertyName,"()=>"+property);
		case "node":
			return this.evalFunction(propertyName,"()=>thisObject.nodeContext.get("+property+")");
		case "flow":
			if(this.flow==null) throw Error("context store may be memoryonly so flow doesn't work")
			return this.evalFunction(propertyName,"()=>thisObject.flow.get("+property+")");
		case "global":
			return this.evalFunction(propertyName,"()=>thisObject.global.get("+property+")");
		case "env":
			return this.evalFunction(propertyName,"()=>env.get("+property+")");
		case "msg":
			return this.evalFunction(propertyName,"(msg)=>msg."+property);
		case "re":
			return this.evalFunction(propertyName,"(msg)=>\\"+property+"\\");
		case "bin":
			return this.evalFunction(propertyName,"(msg)=>{"+property+"}");
		default:
			logger.sendInfo({label:"setData unknown type",action:node.action,propertyType:propertyType,type:node[propertyType]});
			throw Error("unknown type "+node[propertyType])
		}
	} catch(ex) {
		logger.sendError({label:"setup",error:ex.message,stack:ex.stack});
		throw Error(propertyName+" "+ex.message);
	}
}
function argsArray(node,msg) {
	const args=[];
	node.argFunction.forEach(callFunction=> {
		const result=callFunction(msg);
		args.push(result);
	});
	return args;
}
noderedBase.prototype.setSource=function(propertyName){
	const node=this.node;
    if(!node.hasOwnProperty(propertyName)){
		logger.sendWarn({label:"setSource",error:"property on node not found",id:node.id,propertyName:propertyName})
		return this;
	} 
	const getPropertyName="get"+propertyName.substr(0,1).toUpperCase()+propertyName.substr( 1 )
    node[getPropertyName]=this.evalInFunction(propertyName);
	if(logger.active) logger.send({label:"setSource",node:{id:node.id,name:node.name},functionName:getPropertyName,function:node[getPropertyName].toString()});
    return this;
}
noderedBase.prototype.setTarget=function(propertyName){
	const node=this.node;
	const setPropertyName="set"+propertyName.substr(0,1).toUpperCase()+propertyName.substr(1);
    if(!node.hasOwnProperty(propertyName)){
		logger.sendWarn({label:"setTarget",error:"property on node not found",id:node.id,propertyName:propertyName,functionName:setPropertyName})
		return this;
	};
    switch(node[propertyName+"-type"]){
    case "node":
        node[setPropertyName]=this.evalFunction(propertyName,"data=>nodeContext.set("+node[propertyName]+",data)");
        break;
    case "flow":
        if(flow) throw Error("context store may be memoryonly so flow doesn't work")
        node[setPropertyName]=this.evalFunction(propertyName,"data=>flow.set("+node[propertyName]+",data)");
        break;
    case "global":
        node[setPropertyName]=this.evalFunction(propertyName,"data=>global.set("+node[propertyName]+",data)");
        break;
    case "msg":
        node[setPropertyName]=this.evalFunction(propertyName,"(data,msg)=>{msg."+node[propertyName]+"=data;}");
        break;
    default:
        logger.sendWarn({label:"setData unknown type",action:node.action,targetType:node[propertyName+"target-type"]});
		return;
    }
	if(logger.active) logger.send({label:"setTarget",node:{id:node.id,name:node.name},functionName:setPropertyName,function:node[setPropertyName].toString()});
	return this;
}
noderedBase.prototype.setArgs=function(propertyName){
    this.args[propertyName]=[];
    this.args[propertyName].forEach(property=>{
        node.argFunction.push(this.evalInFunction(node,property).bind(this));
    })
	return this;
}
module.exports=noderedBase;