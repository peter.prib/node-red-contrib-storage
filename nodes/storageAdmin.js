const logger = new (require("node-red-contrib-logger"))("Storage Admin");
logger.sendInfo("Copyright 2022 Jaroslav Peter Prib");
const noderedBase=require("./noderedBase.js");
function debugValue(v){
	return typeof (v||"*** undefined ***") === 'function'? v.toString():v;
}

function error(node,message,shortMessage){
	if(logger.active) logger.send({label:"error",node:node.id,error:message,shortMessage:shortMessage});
	node.error(message);
	node.status({fill:"red",shape:"ring",text:shortMessage});
	return this;
}

module.exports = function (RED) {
	function Node(n) {
	    RED.nodes.createNode(this,n);
//		logger.sendInfo({label:"createNode",node:n});
	    const node=Object.assign(this,n);
		node._base=new noderedBase(RED,node);
		try{
			node.status({fill:"yellow",shape:"ring",text:"opening"});
			node.storageDefinitionNode=RED.nodes.getNode(node.storageDefinition);
			if(node.storageDefinitionNode==null) throw Error(node.storageDefinition+" storage definition not found")
			node.storageDefinitionNode.onReady(node,
				()=>node.status({fill:"green",shape:"ring"}),
				(err)=>node.status({fill:"red",shape:"ring",text:err})
			);
			this._base.setSource("action") //.setTarget("target")
		} catch(ex) {
			node._base.error(ex,"Invalid setup "+ex.message);
			return;
		}
		node.on("input",function (msg) {
			try{
				const action=node.getAction();
				if(!["open","close","setAvaliable","setUnavailable"].includes[action])
					throw Error("unknown action: "+action);
				node.storageDefinitionNode[action](
					(data)=>{
//						if(data) node.setTarget(data,msg);
						node.send(msg);
					},
					(ex)=>{
						msg.error=ex.message;
						node.send([null,msg]);
						if(logger.active) logger.send({label:"error",node:node.id,action:node.action,exception:ex.message,stack:ex.stack});
					}
				);
			} catch(ex) {
				msg.error=ex.message;
				node.send([null,msg]);
				if(logger.active) logger.send({label:"error",node:node.id,action:node.action,exception:ex.message,stack:ex.stack});
			}
		});
	}
	RED.nodes.registerType(logger.label, Node);

	RED.httpAdmin.get('/storageAdmin/:id/:action', RED.auth.needsPermission('storageAdmin.write'), function (req, res) {
		if(logger.active) logger.send({label:'httpAdmin.get',parms:req.params})
		const node=RED.nodes.getNode(req.params.id);
		try {
			if(node==null) throw Error("Not authorised");
			if(node.type!==logger.label) throw Error("flow node found but wrong type");
			const action=req.params.action;
			switch (action) {
			case 'closeFile':
			case 'openFile':
			case 'setAvailable':
			case 'setUnavailable':
				node.storageDefinitionNode[action](()=>res.sendStatus(200),(ex)=>{const err=action+" error: "+ex.message;node.warn(err);res.status(500).send(err)})
				node.send({topic:action,payload:{node:{id:node.id,name:node.name}}});
				break;
			default:
				res.status(404).send('request to ' + action + ' failed as action not found');
			}
		} catch (ex) {
			const reason1 = 'Internal Server Error, id: '+req.params.id+" action: "+ req.params.action + ' failed ' + ex.toString()
			node&&node.error(reason1)
			logger.error({label:"httpAdmin.get",error:ex.message,parms:req.params,stack:ex.stack})
			res.status(500).send(reason1)
		}
	})
};