const logger = new (require("node-red-contrib-logger"))("BlockStorage");
logger.sendInfo("Copyright 2022 Jaroslav Peter Prib");
//import { Buffer } from 'node:buffer';
function BlockStorage(){
	Object.assign(this,arguments[0]);
	this.available=false;
	this.closed=true;
	this.queue=[];
	this.onReadyStack=[];
	this.bufferMaxBlocks=100;
	this.cacheBuffer=Buffer.alloc(this.bufferMaxBlocks*4096);
	this.cacheBufferMap=new Uint16Array(this.bufferMaxBlocks);
	//Uint8Array 
	return this;
}
BlockStorage.prototype.error=function(err){
	if(logger.active) logger.send({label:"BlockStorage.error",error:err});
	const error=typeof err=="string"?Error(err):err;
	this.onReadyStack.forEach((action,i,stack)=>{
		try{
			if(action.errorFunction) action.errorFunction.apply(action.object,error);
		} catch(ex){
			logger.error("error in onReadyStack for error "+i+", "+ex.message)
		}
	});
	if(this.errorFunction) this.errorFunction(error)
	else throw error;
	return this;
};
BlockStorage.prototype.fs=require("fs");
BlockStorage.prototype.getStats=function(done,error){
	if(this.available){
		this.getStatsFile(...arguments);
		return this;
	}
	if(logger.active) logger.send({label:"qGetStats",queued:this.queue.length});
	this.queue.push({call:this.getStatsFile,arguments:[arguments]})
	return this;
}
BlockStorage.prototype.onReady=function(object,readyFunction,errorFunction){
	this.onReadyStack.push({object:object,readyFunction:readyFunction,errorFunction:errorFunction});
	if(this.available!==true) return this;
	try{
		if(readyFunction) readyFunction.apply(object);
	} catch(ex){
		logger.error("onReady - onReadyStack "+(this.onReadyStack.length-1)+", "+ex.message)
	}
	return this;
};
BlockStorage.prototype.close=function(done,errorFunction=this.error){
	const thisObject=this;
	this.fs.close(this.fileDescriptor, function(err, stats) {
		if(logger.active) logger.send({label:"fs.close",error:err,stats:stats});
		if(err)	return errorFunction("close failed, "+err);
		thisObject.closed=true;
	})
	return this;
}
BlockStorage.prototype.open=function(done,errorFunction=this.error){
	const thisObject=this;
	const openFlag="r+"
	thisObject.fs.open(thisObject.fileName,openFlag,(err,fileDescriptor)=>{
		if(logger.active) logger.send({label:"fs.open",fileName:thisObject.fileName,error:err});
		if(err) return thisObject.error(err,"Open failed, "+err)
		thisObject.fileDescriptor=fileDescriptor;
		thisObject.closed=false;
		thisObject.fs.fstat(thisObject.fileDescriptor, function(err, stats) {
			if(logger.active) logger.send({label:"fs.fstat",error:err,stats:stats});
			if(err)	return thisObject.error("fstats failed, "+err);
			if(!stats.isFile()) return thisObject.error("not a file");
//			if(!stats.isBlockDevice()) thisObject.error("not a blocked file");
			Object.assign(thisObject,stats);
			thisObject.setAvailable();
			if(done) done();
			/* e.g. of stats
			{
				dev: 2114,
				ino: 48064969,
				mode: 33188,
				nlink: 1,
				uid: 85,
				gid: 100,
				rdev: 0,
				size: 527,
				blksize: 4096,
				blocks: 8,
				atime: Mon, 10 Oct 2011 23:24:11 GMT,
				mtime: Mon, 10 Oct 2011 23:24:11 GMT,
				ctime: Mon, 10 Oct 2011 23:24:11 GMT,
				birthtime: Mon, 10 Oct 2011 23:24:11 GMT
			  }
			*/
		});
	});
	return this;
};
BlockStorage.prototype.read=function(key,done,error){
	if(this.available && this.queue.length==0){
		this.readFile.apply(this,arguments);
		return this;
	}
	this.queue.push({call:this.readFile,arguments:arguments})
	if(logger.active) logger.send({label:"read",queueSize:this.queue.length});
	return this;
}
BlockStorage.prototype.readFile=function(blockNumber,done,error=this.error.bind(this)){
	try{
		const position=blockNumber*this.blksize,
			thisObject=this,
			options={offset:0,length:this.blksize,position:blockNumber*this.blksize};
		if(logger.active) logger.send({label:"BlockStorage.readFile",options:options});
		this.fs.read(this.fileDescriptor,this.cacheBuffer,0,this.blksize,blockNumber*this.blksize, function(err,bytesRead,buffer){
				if(logger.active) logger.send({label:"BlockStorage.readFile done",options:options,err:err,bytesRead:bytesRead});
			if(err){
				error(err);
			} else if(bytesRead!=thisObject.blksize){
				error(Error("only "+bytesRead+" bytes read expecting "+thisObject.blksize+" bytes"));
			} else {
				done(buffer.slice(offset,offset+thisObject.cacheBuffer));
			}
		})
	} catch(ex){
		error(ex)
	}
	return this;
}
BlockStorage.prototype.setAvailable=function(done,error){
	if(logger.active) logger.send({label:"setAvailable"});
	if(this.closed){
		logger.error("setAvailable, is closed")
		error(Error("is closed"))
	}
	this.available=true;
	this.onReadyStack.forEach((action,i,stack)=>{
		try{
			if(action.readyFunction) action.readyFunction.apply(action.object);
		} catch(ex){
			logger.error("setAvailable onReadyStack {i}, "+ex.message)
		}
	});
	while(this.queue.length && this.available) {
		try{
			if(logger.active) logger.send({label:"setAvailable process queue action",queueSize:this.queue.length});
			const action=this.queue.shift();
			action.call.apply(this,action.arguments);
		} catch(ex){
			logger.error("setAvailable process queue, "+ex.message);
		}
	}
	if(done) done();
	return this;
};
BlockStorage.prototype.setErrorFunction=function(errorFunction){
	this.errorFunction=errorFunction;
	return this;
}
BlockStorage.prototype.setUnavailable=function(done){
	if(logger.active) logger.send({label:"setUnavailable"});
	this.available=false;
	this.onReadyStack.forEach((action,i,stack)=>{
		try{
			if(action.errorFunction) action.errorFunction.apply(action.object,["set Unavailable"]);
		} catch(ex){
			logger.error("setUnavailable - onReadyStack "+i+", "+ex.message)
		}
	});
	if(done) done();
};
BlockStorage.prototype.write=function(key,data,done,error){
	if(this.available && this.queue.length==0) {
		this.writeFile.apply(this,arguments);
		return this;
	}
	this.queue.push({call:this.writeFile,arguments:arguments})
	if(logger.active) logger.send({label:"write queued",queueSize:this.queue.length});
	return this;
}
BlockStorage.prototype.writeFile=function(blockNumber,data,done,error=this.error.bind(this)){
	try{
		const thisObject=this,
		offsetCacheBuffer=0;
			position=blockNumber*this.blksize;
		this.cacheBuffer.fill(data,offsetCacheBuffer);
		if(logger.active) logger.send({label:"BlockStorage.writeFile",position:position});
		//	this.fs.write(this.fd, buffer, offset, length, position, callback(err, bytesWritten, buffer)
		this.fs.write(this.fileDescriptor,this.cacheBuffer,offsetCacheBuffer,this.blksize,position, (err, bytesWritten, buffer)=>{
			if(logger.active) logger.send({label:"BlockStorage.writeFile done",blockNumber:blockNumber,position:position,err:err,bytesWritten:bytesWritten});
			if(err){
				error(err)
			} else if(bytesWritten!==thisObject.blksize){
				error(Error("only "+bytesWritten+" bytes written expecting "+thisObject.blksize+" bytes"));
			}else {
				done();
			}		
		})
	} catch(ex){
		error(ex)
	}
	return this;
}
module.exports=BlockStorage;
/*
function DataStructure(){
	this.schema=[];
}
DataStructure.prototype.encode=function(base={}){
	return this.record;
};
DataStructure.prototype.decode=function(record=this.record){
	let base={};
	return base;
};

/*
[
{name:"aProperty",type:DataType.BigInt,offset:99,length:99,endian:}
]
BE->big-endian
LE->little-endian

Const DataType={
	"BigInt64":{byteLength:99,endian:true},
}

buf.readBigInt64BE([offset])
buf.readBigInt64LE([offset])
buf.readBigUInt64BE([offset])
buf.readBigUInt64LE([offset])
buf.readDoubleBE([offset])
buf.readDoubleLE([offset])
buf.readFloatBE([offset])
buf.readFloatLE([offset])
buf.readInt8([offset])
buf.readInt16BE([offset])
buf.readInt16LE([offset])
buf.readInt32BE([offset])
buf.readInt32LE([offset])
buf.readIntBE(offset, byteLength)
buf.readIntLE(offset, byteLength)
buf.readUInt8([offset])
buf.readUInt16BE([offset])
buf.readUInt16LE([offset])
buf.readUInt32BE([offset])
buf.readUInt32LE([offset])
buf.readUIntBE(offset, byteLength)
buf.readUIntLE(offset, byteLength)

*/
